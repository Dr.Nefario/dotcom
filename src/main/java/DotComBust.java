import java.util.ArrayList;

public class DotComBust {

    //declare and Initialize the Variables we need
   private GameHelper helper = new GameHelper();
   private ArrayList<DotCom> dotComsList = new ArrayList<DotCom>();
   private int numOfGuesses = 0;

   private void setUpGame(){

       //make three DotCom objects, give them names, and stick ‘em in the ArrayList
       DotCom one = new DotCom();
       one.setName("Pets.com");
       DotCom two = new DotCom();
       two.setName("eToys.com");
       DotCom three = new DotCom();
       three.setName("Go.com");
       dotComsList.add(one);
       dotComsList.add(two);
       dotComsList.add(three);

       //print brief instructions for user
       System.out.println("");
       System.out.println("");
       System.out.println("");

       for (DotCom dotComToSet : dotComsList){      //repeat with each DotCom in the list
           ArrayList<String> newLocation =  helper.placeDotCom(3); //Ask the helper for a
                                                                           //DotCom location
                                                                           //(an ArrayList of Strings).
           dotComToSet.setLocationCells(newLocation);   //Call the setter method on this
                                                        //DotCom to give it the location you
                                                        //just got from the helper.
       }
   }
   private void startPlaying (){
       while (!dotComsList.isEmpty() ){    //As long as the DotCom list is NOT empty (the ! means NOT,
                                           //it’s the same as (dotComsList.isEmpty() == false).
           String userGuess =  helper.getUserInput("Enter a guess: "); //Get user input.
           checkUserGuess(userGuess) ;  //Call our own checkUserGuess method
       }
       finishGame();   //Call our own finishGame method.
   }
    private void checkUserGuess(String userGuess){
        numOfGuesses++;   //increment the number of guesses the user has made
        String result = "miss"; //assume it’s a ‘miss’, unless told otherwise
        for (DotCom dotComToTest : dotComsList ){ //repeat with all DotComs in the list
            result = dotComToTest.checkYourself(userGuess); //ask the DotCom to check the user
                                                            //guess, looking for a hit (or kill)
            if (result.equals("hit"))
                break; //get out of the loop early, no point in testing  the others
            if (result.equals("kill")){
                dotComsList.remove(dotComToTest); //this guy’s dead, so take him out of the
                break;                            //DotComs list then get out of the loop
            }
        }
        System.out.println(result); //print the result for  the user
    }


    private void finishGame(){ //print a message telling the
                               //user how he did in the game
        System.out.println("All Dot Coms are dead! Your stock is now worthless.");
        if(numOfGuesses <= 18){
            System.out.println("It only took you " + numOfGuesses + " guesses.");
            System.out.println("It only took you " + numOfGuesses + " guesses.");
        }
        else {
            System.out.println("Took you long enough. "+ numOfGuesses + " guesses.");
            System.out.println("Fish are dancing with your options.");
        }
    }
    public static void main (String args[]){
       DotComBust game = new DotComBust();         //create the game object
       game.setUpGame();   //tell the game object to set up the game
       game.startPlaying();  //tell the game object to start the main
                            //game play loop (keeps asking for user
                           //input and checking the guess)
    }

}

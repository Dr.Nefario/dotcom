import java.util.* ;
public class DotCom {

    private ArrayList<String> locationCells; //DotCom’s instance variables:
                                             //- an ArrayList of cell locations
                                             //- the DotCom’s name
    private String name ;

    public void setLocationCells(ArrayList<String> loc){    //A setter method that updates
                                                            //the DotCom’s location.
                                                            //(Random location provided by
                                                            //the GameHelper placeDotCom( )
                                                            //method.)
        loc = locationCells;
    }

    public void setName (String n){
        name = n ;
    }

    public String checkYourself (String userInput){
        String result = "miss";
        int index = locationCells.indexOf (userInput);  //The ArrayList indexOf( ) method in
                                                        //action! If the user guess is one of the
                                                        //entries in the ArrayList, indexOf( )
                                                        //will return its ArrayList location. If
                                                        //not, indexOf( ) will return -1.
        if (index >= 0 ){
            locationCells.remove(index);  //Using ArrayList’s remove( ) method to delete an entry.

            if(locationCells.isEmpty()){  //Using the isEmpty( ) method to see if all
                                          //of the locations have been guessed
                result = "kill";
                System.out.println("You Sunk " + name  );   //Tell the user when a DotCom
                                                            // has been sunk
            }
            else{
                result = "hit";
            }
        }
        return result;  //Return: ‘miss’ or ‘hit’ or ‘kill’
    }


}
